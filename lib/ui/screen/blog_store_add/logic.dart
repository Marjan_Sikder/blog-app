import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:my_app/repositories/all_repository.dart';

import '../../../style/text_style.dart';
import '../base_controller.dart';
import 'state.dart';

class BlogStoreAddLogic extends BaseController {
  final BlogStoreAddState state = BlogStoreAddState();

  List<String> checkFormValidation(){
    List<String> errors = [];
    if(state.titleController.text.isEmpty){
      errors.add('Title Field is empty!');
    }
    if(state.subtitleController.text.isEmpty){
      errors.add('Subtitle Field is empty!');
    }
    if(state.descriptionController.text.isEmpty){
      errors.add('Description Field is empty!');
    }
    if(state.selectDate.value.isEmpty){
      errors.add('Date Field is empty!');
    }
    return errors;
  }

  void onSubmit() async{
    var validation = checkFormValidation();
    if(validation.isEmpty){
      blogStoreApi();
    }else{
      ShowDialogSingleButton('Error', validation.join('\n'),
          onConfirm: (){
            Get.back();
          },
          onCancel: () {
            Get.back();
          });
    }
  }

  void blogStoreApi() async{

    try{
      state.isLoading.value = true;
      var resp = await AllRepository.blogStoreAdd(
        state.titleController.text,
        state.subtitleController.text,
        state.descriptionController.text,
        state.selectDate.value,
      );
      state.isLoading.value = false;
      if(resp.status == 1){
        showSingleCustomDialogBox(Get.context!,''.tr, "${resp.message!.isNotEmpty?resp.message:"Something went wrong"}",null,null,
            onConfirm: (){
              Get.back();
              Get.back();
            });
      }else{

      }
    }catch(e){
      state.isLoading.value = false;

    }
  }
}
