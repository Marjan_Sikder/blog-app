import 'package:flutter/cupertino.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:intl/intl.dart';

import '../base_states.dart';

class BlogStoreAddState extends BaseState {
  BlogStoreAddState() {
    ///Initialize variables
  }

  TextEditingController titleController = TextEditingController(text:'');
  TextEditingController descriptionController = TextEditingController(text:'');
  TextEditingController subtitleController = TextEditingController(text:'');
  var selectDate = Rx<String>(DateFormat('dd-MM-yyyy').format(DateTime.now()));

}
