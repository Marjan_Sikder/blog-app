import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../../../repositories/all_repository.dart';
import '../../../style/text_style.dart';
import '../base_controller.dart';
import 'state.dart';

class BlogUpdateLogic extends BaseController {
  final BlogUpdateState state = BlogUpdateState();

  @override
  void onReady() {
    super.onReady();
    state.blogId = Get.arguments[0];
    state.titleController.text = Get.arguments[1];
    state.subtitleController.text = Get.arguments[2];
    state.descriptionController.text = Get.arguments[3];
    state.selectDate.value = Get.arguments[4];
    _setInitialData();
  }

  void blogUpdateApiCall() async {
    try{
      state.isLoading.value = true;
      var resp = await AllRepository.blogUpdateApi(state.blogId);
      state.isLoading.value = false;
      if(resp.status == 1 ){
        showSingleCustomDialogBox(Get.context!,''.tr, "${resp.message!.isNotEmpty?resp.message:"Something went wrong"}",null,null,
            onConfirm: (){
              Get.back();
              Get.back();
            });
      }else{

      }
    }catch(e){
      state.isLoading.value = false;
      update();
      print('?????????????????');
      print(e.toString());
      showSingleCustomDialogBox(Get.context!,'Error'.tr, e.toString(),null,null,
          onConfirm: (){
            Get.back();
          });
    }

  }

  _setInitialData(){
    state.titleController.text = state.titleController.text?? 'N/A';
    state.subtitleController.text = state.subtitleController.text?? 'N/A';
    state.descriptionController.text = state.descriptionController.value.text?? 'N/A';
    state.tagController.text = state.tagController.text?? 'N/A';
    state.selectDate.value = state.selectDate.value ??'N/A';
    update();
  }
}
