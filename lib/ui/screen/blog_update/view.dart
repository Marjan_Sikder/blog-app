import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'logic.dart';
import '../../../style/text_style.dart';
import '../../../style/theme.dart';
import '../../../utils/CustomDatePicker.dart';
import '../../customWidgets/CustomTextField.dart';
import '../../customWidgets/customActionBar.dart';
import '../../customWidgets/root_containers.dart';


class BlogUpdatePage extends StatelessWidget {
  BlogUpdatePage({Key? key}) : super(key: key);

  final logic = Get.put(BlogUpdateLogic());
  final state = Get.find<BlogUpdateLogic>().state;

  @override
  Widget build(BuildContext context) {
    return RootContainers.instance.getScaffoldRootContainer(
      Scaffold(
        body: Container(
          color: AppColors.white,
          height: Get.height,
          child: Column(
            children: [
              CustomActionbar('Blog Update'.tr, () {
                Get.back();
              }),
              Expanded(
                child:Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(
                    width: Get.width,
                    decoration: RootContainers.instance.getBoxDecorations(
                        AppColors.priceShowColor, 5),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          SizedBox(height: 10,),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10.0),
                              child: Text('■ Title :',
                                style: getTextStyle(15, FontWeight.normal, AppColors.greenButton),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),),
                          CustomTextFields(
                              "",
                              state.titleController,
                              TextInputType.text),
                          SizedBox(height: 10,),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10.0),
                              child: Text('■ Subtile :',
                                style: getTextStyle(15, FontWeight.normal, AppColors.greenButton),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),),
                          CustomTextFields(
                              "",
                              state.subtitleController,
                              TextInputType.text),
                          SizedBox(height: 10,),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10.0),
                              child: Text('■ Description :',
                                style: getTextStyle(15, FontWeight.normal, AppColors.greenButton),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                            child: Container(
                              padding: EdgeInsets.only(left: 20,right: 20,bottom: 5),
                              decoration: new BoxDecoration(
                                  color: AppColors.white,
                                  borderRadius: new BorderRadius.only(
                                    topLeft: const Radius.circular(5.0),
                                    topRight: const Radius.circular(5.0),
                                    bottomLeft: const Radius.circular(5.0),
                                    bottomRight: const Radius.circular(5.0),
                                  )
                              ),
                              child: TextField(
                                  controller: state.descriptionController,
                                  maxLines: 8,
                                  minLines: 8,
                                  decoration: InputDecoration(
                                    border: InputBorder
                                        .none,
                                    hintText: 'Write a comment..'.tr,
                                    hintStyle: getTextStyle(14,FontWeight.normal, Colors.grey),
                                  ),
                                  style: getTextStyle(
                                      16,
                                      FontWeight.normal,
                                      Colors.black)
                              ),
                            ),
                          ),
                          SizedBox(height: 10,),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10.0),
                              child: Text('■ Date :',
                                style: getTextStyle(15, FontWeight.normal, AppColors.greenButton),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),),
                          SizedBox(height: 10,),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            child: Container(
                              width: Get.width,
                              decoration: BoxDecoration(
                                  color: AppColors.white,
                                  borderRadius: new BorderRadius.only(
                                    topLeft: const Radius.circular(5.0),
                                    topRight: const Radius.circular(5.0),
                                    bottomLeft: const Radius.circular(5.0),
                                    bottomRight: const Radius.circular(5.0),
                                  )
                              ),
                              padding: EdgeInsets.only(left: 20, right: 20),
                              height: 50,
                              child: CustomDatePicker((text) {
                                state.selectDate.value = text;
                              }, DateTime(DateTime.now().year + 100),
                                  DateTime(DateTime.now().year - 100),
                                  DateTime(DateTime.now().year)),
                            ),
                          ),
                          SizedBox(height: 10,),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: AppColors.takaColor, // background
                  onPrimary: Colors.white, // foreground
                ),
                onPressed: () {
                  logic.blogUpdateApiCall();
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 30.0,right: 30),
                  child: Text('Update'.tr,
                      style: buttonTextStyle),
                ),
              ),
              SizedBox(
                height: 10,
              )
            ],
          ),
        ),
      ),
    );
  }
}
