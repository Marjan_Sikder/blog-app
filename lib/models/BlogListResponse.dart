class BlogListResponse {
  Data data;
  String message;
  int status;

  BlogListResponse({
    required this.data,
    required this.message,
    required this.status,
  });

  factory BlogListResponse.fromJson(Map<String, dynamic> json) {
    return BlogListResponse(
      data: Data.fromJson(json['data']),
      message: json['message'],
      status: json['status'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'data': data.toJson(),
      'message': message,
      'status': status,
    };
  }
}

class Data {
  Blogs blogs;

  Data({
    required this.blogs,
  });

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      blogs: Blogs.fromJson(json['blogs']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'blogs': blogs.toJson(),
    };
  }
}

class Blogs {
  List<Blog> data;


  Blogs({
    required this.data,
  });

  factory Blogs.fromJson(Map<String, dynamic> json) {
    var list = json['data'] as List;
    List<Blog> dataList = list.map((i) => Blog.fromJson(i)).toList();
    return Blogs(
      data: dataList,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'data': data.map((e) => e.toJson()).toList(),
    };
  }
}

class Blog {
  int id;
  int categoryId;
  String title;
  String subTitle;
  String slug;
  String description;
  String date;
  String status;
  String createdAt;
  String updatedAt;

  Blog({
    required this.id,
    required this.categoryId,
    required this.title,
    required this.subTitle,
    required this.slug,
    required this.description,
    required this.date,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
  });

  factory Blog.fromJson(Map<String, dynamic> json) {
    return Blog(
      id: json['id'],
      categoryId: json['category_id'],
      title: json['title'],
      subTitle: json['sub_title'],
      slug: json['slug'],
      description: json['description'],
      date: json['date'],
      status: json['status'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'category_id': categoryId,
      'title': title,
      'sub_title': subTitle,
      'slug': slug,
      'description': description,
      'date': date,
      'status': status,
      'created_at': createdAt,
      'updated_at': updatedAt,
    };
  }
}

