/// message : "Blog Save Success"
/// status : 1

class BlogAddResp {
  BlogAddResp({
      String? message, 
      num? status,}){
    _message = message;
    _status = status;
}

  BlogAddResp.fromJson(dynamic json) {
    _message = json['message'];
    _status = json['status'];
  }
  String? _message;
  num? _status;
BlogAddResp copyWith({  String? message,
  num? status,
}) => BlogAddResp(  message: message ?? _message,
  status: status ?? _status,
);
  String? get message => _message;
  num? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = _message;
    map['status'] = _status;
    return map;
  }

}