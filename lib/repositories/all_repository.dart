import '../api/api_base_helper.dart';
import '../config/server_addresses.dart';
import '../models/BlogAddResp.dart';
import '../models/BlogListResponse.dart';
import '../models/BlogUpdateResponse.dart';
import '../models/DeleteResponse.dart';
import '../models/base_response.dart';
import '../models/login_req.dart';
import '../models/login_resp.dart';
import '../utils/PreferenceUtils.dart';
import '../models/SignInResponse.dart';


class AllRepository {
  static ApiBaseHelper _helper = ApiBaseHelper();

//admin log in api:
  static Future<SignInResponse> adminLogin(String email,String password) async{
    Map<String, dynamic> req = {"email": email,"password":password};
    final response = await _helper.post(ServerAddresses.userLogin,req);
    return SignInResponse.fromJson(response);
  }


//get Blog List api :
  static Future<BlogListResponse> blogList() async{
    PreferenceUtils.init();
    final response = await _helper.get(ServerAddresses.blogList,token:'Bearer');
    return BlogListResponse.fromJson(response);
  }


//delete api :
  static Future<DeleteResponse> deleteBlog(int id) async{
    Map<String,dynamic> req = {'id':id};
    final response = await _helper.delete('/api/admin/blog-news/delete/$id',req,token: 'Bearer');
    return DeleteResponse.fromJson(response);
  }


//blog Store api :
  static Future<BlogAddResp> blogStoreAdd(
      String title,
      String subTitle,
      String description,
      String date,
      ) async{
    Map<String, dynamic> req = {
      "title": title,
      "sub_title": subTitle,
      "description": description,
      "date": date,

    };
    final response = await _helper.post(ServerAddresses.blogStore,req,token: 'Bearer');
    return BlogAddResp.fromJson(response);
  }

//blog update api :
  static Future<BlogUpdateResponse> blogUpdateApi(int id) async{
    Map<String,dynamic> req = {'id':id};
    final response = await _helper.post('/api/admin/blog-news/update/$id',req,token: 'Bearer');
    return BlogUpdateResponse.fromJson(response);
  }


}



